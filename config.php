<?php
    // MySQL
    $connect = new mysqli(Site::$dbhost, Site::$dbuser, Site::$dbpass);
    if($connect->connect_error) {
        die('Connection failed: '.$connect->connect_error);
    }

    $connect->set_charset('utf8');

    // Create database
    $sql='CREATE DATABASE IF NOT EXISTS ' . Site::$dbname;
    if($connect->query($sql) === TRUE) {
        $connect->select_db(Site::$dbname);
        // Create table
        $sql = 'CREATE TABLE IF NOT EXISTS `products` ( `id` INT NOT NULL AUTO_INCREMENT , `sku` VARCHAR(10) NOT NULL , `name` VARCHAR(255) NOT NULL , `price` DECIMAL(17,2) NOT NULL , `property` VARCHAR(64) NOT NULL , `prop_value` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;';
        Site::sendQuery($sql);
    } else {
        echo 'Error creating database: ' . $connect->error;
    }

    // Display page configuration

    /* Scan directory for php files and get an array of their names without ".php"
    Save pages directory to the static variable in Site class */
    $pages = Site::scanDirectoryForPages('pages');

    // Get selected page from list and choose default if selection is empty, choose 404 if page not in list
    $actual_page = Site::getSelectedPage($pages, Site::$default_page); 

    // Display selected page
    Site::displaySelectedPage($actual_page); 
?>