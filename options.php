<?php
    // Set up a new website
    Site::$name = 'Nproshop'; // Site name
    Site::$theme = 'default'; // Site theme
    Site::$default_page = 'product_list'; // Name of the home page file without ".php"

    // MySQL
    Site::$dbhost = 'localhost'; // Hostname
    Site::$dbuser = 'root'; // Username
    Site::$dbpass = ''; // Password
    Site::$dbname = 'nproshopdb'; // Database name
?>