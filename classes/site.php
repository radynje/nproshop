<?php
    // Actual link of the site
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

    class Site {
        public static $name; // Site name
        public static $theme; // Site theme
        public static $head; // Site beginning
        public static $dbhost; // Database host
        public static $dbuser; // Database user
        public static $dbpass; // Database password
        public static $dbname; // Database name
        public static $default_page; // Default page if page not selected in GET query
        public static $page_folder; // Folder where are all pages

        // Get standart beginning of the site
        public static function getHead() {
            Site::$head = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
                <head>
                    <meta charset="UTF-8"/>
                    <link rel="stylesheet" href="themes/'.Site::$theme.'/style.css" />';
            return Site::$head;
        }

        // Get selected page and choose default if selection is empty, choose 404 if page not in list
        public static function getSelectedPage($pages, $default_page) {
            $page = isset($_GET['page']) ? $_GET['page'] : $default_page;

            if(in_array($page, $pages)) {
                foreach($pages as $p) {
                    if($page == $p) {
                        return $page;
                    } else {
                        continue;
                    }
                    unset($p);
                }
            } else {
                return '404';
            }
        }

        // Get an array of page names
        public static function scanDirectoryForPages($directory) {
            // Check directory for php files, get array of them and then reindex to get rid of ".." and "." indexes
            $pages = array_values(array_filter(scandir($directory), function($item) {
                return !is_dir($directory . $item);
            }));

            // Delete ".php" from name of each element
            for($i=0; $i<count($pages); $i++) {
                $pages[$i] = str_replace('.php','',$pages[$i]);
            }

            Site::$page_folder = $directory; // Save pages directory
            return $pages;
        }

        // include './' . Site::$page_folder . '/' . Site::$page . '.php';

        public static function displaySelectedPage($page) {
            include './' . Site::$page_folder . '/' . $page . '.php';
        }

        // Change a GET query
        public static function getQueryEdit($key, $value) {
            global $actual_link;
            if (strpos($actual_link, '?') === false) {
                $result = ($actual_link .'?'. $key .'='. $value);
            } else {
                if(strpos($actual_link, $key) === false) {
                    $result = $actual_link.'&'.$key.'='.$value;
                } else {
                    $result = str_replace($key.'='.$_GET[$key], $key.'='.$value, $actual_link);
                }
            }
            return ($result);
        }

        public static function sendQuery($sql) {
            global $connect;
            if($connect->query($sql) === TRUE) {
                echo '';
            } else {
                echo 'Error: ' . $connect->error;
            }
        }
    }