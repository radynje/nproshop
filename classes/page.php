<?php
    // Requires Site class
    class Page {
        public $title; // Title of the page
        public $header_elements; // Dynamic elements in header
        public $status_message; // Status text of page's action

        // Declare page and set it's title
        public function __construct($title) {
            $this->title = $title;
            echo Site::getHead().'<title>'.$this->title.'</title></head>';
        }

        // Open header
        function startHeader() {
            echo '<body><header>';
            ob_start();
        }

        // Close header and display it
        function endHeader() {
            $this->header_elements = ob_get_contents();
            ob_end_clean();
            include 'themes/'.Site::$theme.'/header.php';
            echo '</header>';
        }

        // Start content
        public function startContent() {
            echo '<main>';
        }

        // Close content
        public function endContent() {
            echo '</main></body></html>';
        }
    }
?>