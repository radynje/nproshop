<?php
    class Furniture extends Product {
        public function __construct($sku, $name, $price, $type) {
            parent::__construct($sku, $name, $price, $type);
            $this->property = '"Dimensions"'; // Name must be written in double quotes to correct MySQL query
            $this->prop_value = '"'.$_POST['height'].'x'.$_POST['width'].'x'.$_POST['length'].'"';
        }
    }
?>