<?php
    // Requires Product class

    class Shop {
        // Get products from database
        public static function getProducts() {
            global $connect;
            $sql = 'SELECT * FROM `products` order by `id` DESC';
            $result = $connect->query($sql);
            return $result;
        }

        // Display products
        public static function displayProducts($products) {
            if($products->num_rows > 0) {
                while($row = $products->fetch_assoc()) {
                    include './themes/'.Site::$theme.'/shop/product_view.php';
                }
            }
        }

        public static function getStatusOfDeleteProducts() {
            $status_message;
            if(!empty($_POST['product'])) {
                $num = count($_POST['product']);
                if($num > 1) {
                    $status_message = $num. ' elements were deleted';
                } else {
                    $status_message = '1 element was deleted';
                }
            } else {
                $status_message = 'No elements selected';
            }
            return $status_message;
        }

        // Delete selected products
        public static function deleteProducts() {
            global $connect;
            if(!empty($_POST['product'])) {
                $num = count($_POST['product']);
                for($i=0; $i < $num; $i++) {
                    $sql = 'DELETE FROM `products` WHERE `sku`="'.$_POST['product'][$i].'"';
                    if($connect->query($sql) !== FALSE) {
                        echo '';
                    } else {
                        echo $connect->error;
                    }
                }
            } else {
                echo '';
            } 
        }

        // Create product and add it to database
        public static function addProduct($type, $sku, $name, $price) {
            $product = new $type($sku, $name, $price, $type);
            $product->addToDatabase();
        }
    }
?>