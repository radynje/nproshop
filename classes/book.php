<?php
    class Book extends Product {
        public function __construct($sku, $name, $price, $type) {
            parent::__construct($sku, $name, $price, $type);
            $this->property = '"Weight"'; // Name must be written in double quotes to correct MySQL query
            $this->prop_value = '"'.$_POST['weight'].' KG"';
        }
    }
?>