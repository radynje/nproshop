<?php
    abstract class Product {
        public $sku; // SKU
        public $name; // Name of the product
        public $price; // Price
        public $type; // Type of the product
        public $property; // Property name
        public $prop_value; // Property value

        // Create product
        protected function __construct($sku, $name, $price, $type) {
            $this->sku = '"'.$sku.'"';
            $this->name = '"'.$name.'"';
            $this->price = $price;
            $this->type = '"'.$type.'"';
        }

        // Add product to database
        public function addToDatabase() {
            global $connect;
            $sql = 'INSERT INTO `products` (`sku`, `name`, `price`, `property`, `prop_value`)
                    VALUES ('.$this->sku.','.$this->name.','.$this->price.','.$this->property.','.$this->prop_value.')';
            if($connect->query($sql) === TRUE) {
                echo '';
            } else {
                echo $connect->error;
            }
        }
    }
?>