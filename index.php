<?php
    // Load classes
    spl_autoload_register(function ($class_name) {
        include 'classes/' . $class_name . '.php';
    });
    
    require 'options.php'; // Options
    require 'config.php'; // Configuration
?>