<?php
    $product_list = new Page('Product list');

    // Delete action handler
    if(isset($_POST['delete'])) {
        Shop::deleteProducts(); // delete products and set status message in page that is method variable
        $product_list->status_message = Shop::getStatusOfDeleteProducts(); // display the number of deleted products in status
    }

    $product_list->startHeader();
?>
<form id="productlist" method="post" action="" style="display: inline-block;">
<input class="header-button" type="submit" name="delete" value="Delete" />
<a class="header-button" href="<?php echo Site::getQueryEdit('page', 'add_product') ?>">New product</a> 
<?php 
    $product_list->endHeader();
    $product_list->startContent();

    $products = Shop::getProducts();
    Shop::displayProducts($products);
?>
</form>
<?php
    $product_list->endContent();
?>