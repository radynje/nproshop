<?php
    // Declare new page and it's title
    $page_not_found = new Page('?');

    // Open header html
    $page_not_found->startHeader();
    // Close header html
    $page_not_found->endHeader();

    // Start content
    $page_not_found->startContent();
?>
<h2>404</h2><br />
Page not found
<?php
    // End content
    $page_not_found->endContent();
?>