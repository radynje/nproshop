<?php
    // Declare new page and it's title
    $add_product = new Page('Product add');

    // Add product action handler
    if(isset($_POST['submit'])) {
        Shop::addProduct($_POST['type'], $_POST['sku'], $_POST['prodname'], $_POST['price']);
        $add_product->status_message = 'Product has been added successfuly';
    }

    // Open header html
    $add_product->startHeader();
?>
<a class="header-button" href="<?php echo Site::getQueryEdit('page', 'product_list') ?>">Back to the list</a> 
<?php 
    // Close header html
    $add_product->endHeader(); 
    // Start content
    $add_product->startContent();
?>
<article>
<form id="addproduct" method="post" action="">
    <table>
        <tr>
            <td>SKU: </td>
            <td><input id="sku" name="sku" type="text" placeholder="TI9512758" required /><br /></td>
        </tr>
        <tr>
            <td>Name: </td>
            <td><input id="prodname" name="prodname" type="text" placeholder="Big sofa" required /></td>
        </tr>
        <tr>
            <td>Price: </td>
            <td><input id="price" name="price" type="text" placeholder="15.92" required /></td>
        </tr>
        <tr>
            <td>Type: </td>
            <td>
                <select id="type" name="type" style="width: 100%;" required >
                    <option selected disabled></option>
                    <option value="DVD">DVD-disc</option>
                    <option value="Book">Book</option>
                    <option value="Furniture">Furniture</option>
                </select>
            </td>
        </tr>
    </table>
    <div class="dynamic-block" id="dynaddblock">
        <table id="book" style="display:none;">
            <tr>
                <td>Weight: </td>
                <td><input type="text" name="weight" id="weight"/> KG</td>
            </tr>
        </table>
        <table id="dvd-disc" style="display:none;">
            <tr>
                <td>Size: </td>
                <td><input type="text" name="size" id="size"/> MB</td>
            </tr>
        </table>
        <table id="furniture" style="display:none;">
            <tr>
                <td>Height: </td>
                <td><input type="text" name="height" id="height"/> CM</td>
            </tr>
            <tr>
                <td>Width: </td>
                <td><input type="text" name="width" id="width"/> CM</td>
            </tr>
            <tr>
                <td>Length: </td>
                <td><input type="text" name="length" id="length"/> CM</td>
            </tr>
        </table>
    </div><br />
    <input type="submit" id="submit" name="submit" value="Save"/>
</form>
</article>
<script>
    // Show active category fields and hide others
    document.getElementById('type').onchange = function() {
        switch(document.getElementById('type').value) {
            case 'DVD':
                document.getElementById('dynaddblock').style.display = 'inline-block';
                document.getElementById('dvd-disc').style.display = 'table-row';
                document.getElementById('book').style.display = 'none';
                document.getElementById('furniture').style.display = 'none';
                document.getElementById('size').required = true;
                document.getElementById('weight').required = false;
                document.getElementById('height').required = false;
                document.getElementById('width').required = false;
                document.getElementById('length').required = false;
                break;
            case 'Book':
                document.getElementById('dynaddblock').style.display = 'inline-block';
                document.getElementById('dvd-disc').style.display = 'none';
                document.getElementById('book').style.display = 'table-row';
                document.getElementById('furniture').style.display = 'none';
                document.getElementById('size').required = false;
                document.getElementById('weight').required = true;
                document.getElementById('height').required = false;
                document.getElementById('width').required = false;
                document.getElementById('length').required = false;
                break;
            case 'Furniture':
                document.getElementById('dynaddblock').style.display = 'inline-block';
                document.getElementById('dvd-disc').style.display = 'none';
                document.getElementById('book').style.display = 'none';
                document.getElementById('furniture').style.display = 'table-row';
                document.getElementById('size').required = false;
                document.getElementById('weight').required = false;
                document.getElementById('height').required = true;
                document.getElementById('width').required = true;
                document.getElementById('length').required = true;
                break;
        }
    }
</script>
<?php
    // End content
    $add_product->endContent();
?>